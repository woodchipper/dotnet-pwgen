﻿using System;
using System.Web.Security;

namespace ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)
        {
            string strOrigString = "";

            Console.Write("Enter new password: ");
            strOrigString = Console.ReadLine();
            String strHashedString = FormsAuthentication.HashPasswordForStoringInConfigFile(strOrigString, "md5");
            Console.Write("Your new password is: ");
            Console.Write(strHashedString.Substring(0, 10));
            Console.Read();
        }
    }
}
